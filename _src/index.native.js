import React from 'react';
import { AppRegistry } from 'react-native';
import { Provider } from 'react-redux';

import AppStore from '@store';
import AppNavigator from '@navigator/AppNavigator';

class TeacherTestApp extends React.Component {
    render() {
        console.disableYellowBox = true;
        return (
            <Provider store={AppStore}>
                <AppNavigator />
            </Provider>
        );
    }
}

AppRegistry.registerComponent('TeacherTestApp', () => TeacherTestApp);

