import { combineReducers } from 'redux';
import { NavigationActions } from 'react-navigation';

import { AppNavigator } from '@navigator/AppNavigator';
import { initialNavState } from '@navigator';

function nav(state = initialNavState, action) {
    switch (action.type) {
        case 'Login':
            return AppNavigator.router.getStateForAction(NavigationActions.navigate({ routeName: 'QuestFirst' }), state);
        case 'Logout':
            return AppNavigator.router.getStateForAction(NavigationActions.back(), state);
        default:
            return AppNavigator.router.getStateForAction(action, state);
    }
}

const initialAuthState = { isLoggedIn: true };

function auth(state = initialAuthState, action) {
    switch (action.type) {
        case 'Login':
            return { ...state, isLoggedIn: true };
        case 'Logout':
            return { ...state, isLoggedIn: false };
        default:
            return state;
    }
}

const AppReducer = combineReducers({
    nav,
    auth,
});

export default AppReducer;