import { createStore } from 'redux';
import AppReducer from '@reducers/AppReducer';

const AppStore = createStore(AppReducer);

export default AppStore;