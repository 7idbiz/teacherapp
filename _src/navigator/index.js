import {
    QuestFirstScreen,
    QuestSecondScreen,
} from '@screens';
import styles from '@styles';

const RouteConfigs = {
    QuestFirst: {
        path: 'QuestFirst',
        title: '',
        screen: QuestFirstScreen,
        // navigationOptions: ({navigation}) => ({}),
    },
    QuestSecond: {
        path: 'QuestSecond',
        title: '',
        screen: QuestSecondScreen,
        // navigationOptions: ({navigation}) => ({}),
    },
};

const initialNavState = {
    index: 0,
    routes: [
        { key: 'Init_0', routeName: 'QuestFirst' },
    ],
};

const NavigatorConfig = {
    initialRouteName: 'QuestFirst',
    navigationOptions: ({navigation}) => ({
        headerStyle: styles.screenHeaderBG,
        headerTitleStyle:  styles.screenHeaderTitle,
        // title: `${RouteConfigs[navigation.state.routeName].title || "Default Title for all screens"} `,
    }),
    cardStyle: styles.screenContainerBG,
    headerMode:'screen', // 'float' | 'screen' | 'none'
    mode:'modal', // 'card' | 'modal'
};

export {
    initialNavState,
    RouteConfigs,
    NavigatorConfig,
}