import React from 'react';
import {
    View,
} from 'react-native';
import styles from '@styles';

class EmptyItem extends React.Component {
    render() {
        return (
            <View style={styles.screenContainerEmptyItem}/>
        );
    };
}

export default EmptyItem;