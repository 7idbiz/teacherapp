import React from 'react';
import {
    View,
} from 'react-native';

import {
    CustomCheckBoxComp,
} from '@components';
import styles from '@styles';

class QuestSecondListComp extends React.Component {
    constructor(props) {
        super(props);
    };

    render() {
        return (
            <View style={[styles.textInputContainer, styles.textInputColumnContainer]}>
                <View style={{flex:2}}>
                    <CustomCheckBoxComp title="Nusery" />
                    <CustomCheckBoxComp title="Reception" />
                    <CustomCheckBoxComp title="Year 1" />
                    <CustomCheckBoxComp title="Year 2" />
                    <CustomCheckBoxComp title="Year 3" />
                    <CustomCheckBoxComp title="Year 4" />
                    <CustomCheckBoxComp title="Year 5" />
                    <CustomCheckBoxComp title="Year 6" />
                </View>
                <View style={{flex:2}}>
                    <CustomCheckBoxComp title="Year 7" />
                    <CustomCheckBoxComp title="Year 8" />
                    <CustomCheckBoxComp title="Year 9" />
                    <CustomCheckBoxComp title="Year 10" />
                    <CustomCheckBoxComp title="Year 11" />
                    <CustomCheckBoxComp title="Year 12" />
                    <CustomCheckBoxComp title="Year 13" />
                </View>
            </View>
        );
    };
}

export default QuestSecondListComp;