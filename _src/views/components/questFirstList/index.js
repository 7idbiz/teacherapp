import React from 'react';
import {
    View,
    TextInput,
} from 'react-native';
import styles from '@styles';
import {
    PLACE_HOLDER_TEXT_COLOR_EMPTY,
    PLACE_HOLDER_TEXT_COLOR_ERROR,
} from '@assets/theme';

class QuestFirstListComp extends React.Component {
    constructor(props) {
        super(props);
    };

    focusNextField = (nextField) => {
        this.refs[nextField].focus();
    };

    render() {
        return (
            <View style={styles.textInputContainer}>
                <TextInput
                    ref="1"
                    onSubmitEditing={() => this.focusNextField('2')}
                    placeholder="First Line"
                    returnKeyType="next"
                    style={styles.textInputDefault}
                    placeholderTextColor={PLACE_HOLDER_TEXT_COLOR_EMPTY}
                    underlineColorAndroid="transparent"
                />
                <TextInput
                    ref="2"
                    onSubmitEditing={() => this.focusNextField('3')}
                    placeholder="Second Line"
                    returnKeyType="next"
                    style={styles.textInputDefault}
                    placeholderTextColor={PLACE_HOLDER_TEXT_COLOR_EMPTY}
                    underlineColorAndroid="transparent"
                />
                <TextInput
                    ref="3"
                    onSubmitEditing={() => this.focusNextField('4')}
                    placeholder="Town / City"
                    returnKeyType="next"
                    style={styles.textInputDefault}
                    placeholderTextColor={PLACE_HOLDER_TEXT_COLOR_EMPTY}
                    underlineColorAndroid="transparent"
                />
                <TextInput
                    ref="4"
                    // onSubmitEditing={() => this.focusNextField('2')}
                    placeholder="Postcode"
                    returnKeyType="done"
                    style={styles.textInputDefault}
                    placeholderTextColor={PLACE_HOLDER_TEXT_COLOR_EMPTY}
                    underlineColorAndroid="transparent"
                />
            </View>
        );
    };
}

export default QuestFirstListComp;