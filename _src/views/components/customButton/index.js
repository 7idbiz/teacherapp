import React from 'react';
import {
    TouchableHighlight,
    Text,
    View,
} from 'react-native';

import {
    BUTTON_SUBMIT_UNDERLAY_COLOR,
} from '@assets/theme';
import styles from '@styles';

class CustomButtonComp extends React.Component {
    constructor(props) {
        super(props);
    };

    render() {
        return (
            <View style={{flex:1}}>
                <TouchableHighlight
                    onPress={this.props.onPress}
                    style={styles.customButton}
                    underlayColor={BUTTON_SUBMIT_UNDERLAY_COLOR}
                >
                    <Text style={styles.customButtomTitle}>
                        {this.props.title}
                    </Text>
                </TouchableHighlight >
            </View>
        );
    };
}

export default CustomButtonComp;