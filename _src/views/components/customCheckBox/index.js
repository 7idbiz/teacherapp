import React from 'react';
import {
    CheckboxField
} from 'react-native-checkbox-field';

import {
    CHECK_BOX_DEFAULT_COLOR,
    CHECK_BOX_SELECTED_COLOR,
    CHECK_BOX_DISABLED_COLOR,
} from '@assets/theme';
import styles from '@styles';

class CustomCheckBoxComp extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            selected: (props.selected=="true"),
            fieldLabel: props.title
        };

        this.selectCheckbox = this.selectCheckbox.bind(this);
    }

    selectCheckbox() {
        this.setState({
            selected: !this.state.selected
        });
    }

    render() {
        return (
            <CheckboxField
                labelSide="right"
                label={this.state.fieldLabel}
                onSelect={this.selectCheckbox}
                selected={this.state.selected}
                disabled={this.props.disabled=="true"}
                defaultColor={CHECK_BOX_DEFAULT_COLOR}
                selectedColor={CHECK_BOX_SELECTED_COLOR}
                disabledColor={CHECK_BOX_DISABLED_COLOR}
                containerStyle={styles.checkboxFieldContainer}
                labelStyle={styles.checkboxFieldLabel}
                checkboxStyle={styles.checkboxDefault}
            />
        )
    }
}

export default CustomCheckBoxComp;