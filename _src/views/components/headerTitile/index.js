import React from 'react';
import {
    Text,
    View,
    Image,
} from 'react-native';
import styles from '@styles';

class FormHeaderTitleComp extends React.Component {
    constructor(props) {
        super(props);
    };

    render() {
        return (
            <View style={styles.formHeaderContainer}>
                <Image
                    source={require('@assets/images/logo.png')}
                    style={styles.formHeaderImage}
                />
                <Text style={styles.formHeaderTitle}>
                    {this.props.title}
                </Text>
            </View>
        );
    };
}

export default FormHeaderTitleComp;