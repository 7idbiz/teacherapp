import React, { PropTypes } from 'react';
import { connect } from 'react-redux';
import {
    View,
    Text,
    ScrollView,
} from 'react-native';
import {
    KeyboardAwareScrollView
} from 'react-native-keyboard-aware-scroll-view';
import {
    CustomButtonComp,
    EmptyItem,
    FormHeaderTitleComp,
    QuestFirstListComp,
} from '@components';
import styles from '@styles';

class QuestFirstScreen extends React.Component {
    constructor(props) {
        super(props);
    };

    render() {
        if (this.props.isLoggedIn) {
            return (
                <KeyboardAwareScrollView>
                    <View style={[styles.screenContainerMaxHeight, styles.screenContainerCenter]}>
                        <EmptyItem />
                        <ScrollView>
                            <View style={styles.formContainer}>
                                <FormHeaderTitleComp
                                    title="What's your address?" />
                                <QuestFirstListComp />
                                <CustomButtonComp
                                    title="Next"
                                    onPress={()=>(this.props.navigation.navigate('QuestSecond'))}
                                />
                            </View>
                        </ScrollView>
                        <EmptyItem />
                    </View>
                </KeyboardAwareScrollView>
            );
        } else {
            return (
                <View style={[styles.screenContainerBG, styles.screenContainerEmptyItem]}>
                    <Text style={styles.formHeaderTitle}>Please log in</Text>
                </View>
            )
        }
    };
}

QuestFirstScreen.propTypes = {
    isLoggedIn: PropTypes.bool.isRequired,
};

const mapStateToProps = state => ({
    isLoggedIn: state.auth.isLoggedIn,
});

export default connect(mapStateToProps)(QuestFirstScreen);