import React, { PropTypes } from 'react';
import {
    View,
    Text,
    ScrollView,
} from 'react-native';
import {
    KeyboardAwareScrollView
} from 'react-native-keyboard-aware-scroll-view';

import { connect } from 'react-redux';

import {
    CustomButtonComp,
    EmptyItem,
    FormHeaderTitleComp,
    QuestSecondListComp,
} from '@components';
import styles from '@styles';

class QuestSecondScreen extends React.Component {
    constructor(props) {
        super(props);
    };

    render() {
        if (this.props.isLoggedIn) {
            return (
                <KeyboardAwareScrollView>
                    <View style={[styles.screenContainerMaxHeight, styles.screenContainerCenter]}>
                        <EmptyItem />
                        <ScrollView>
                            <View style={styles.formContainer}>
                                <FormHeaderTitleComp
                                    title="Whats year groups are you comfortable teaching?"
                                />
                                <QuestSecondListComp />
                                <CustomButtonComp
                                    title="Next"
                                    onPress={()=>(this.props.navigation.navigate('QuestFirst'))}
                                />
                            </View>
                        </ScrollView>
                        <EmptyItem />
                    </View>
                </KeyboardAwareScrollView>
            );
        } else {
            return (
                <View style={[styles.screenContainerBG, styles.screenContainerEmptyItem]}>
                    <Text style={styles.formHeaderTitle}>Please log in</Text>
                </View>
            )
        }
    };
}

QuestSecondScreen.propTypes = {
    isLoggedIn: PropTypes.bool.isRequired,
};

const mapStateToProps = state => ({
    isLoggedIn: state.auth.isLoggedIn,
});

export default connect(mapStateToProps)(QuestSecondScreen);