import QuestFirstScreen from './QuestFirst';
import QuestSecondScreen from './QuestSecond';

export {
    QuestFirstScreen,
    QuestSecondScreen,
}