import {
    StyleSheet,
    Dimensions,
    Navigator,
} from "react-native";

import {
    SCREEN_CONTAINER_BACKGROUND_COLOR,
    SCREEN_HEADER_BACKGROUND_COLOR,
    SCREEN_HEADER_TITLE,

    BUTTON_SUBMIT_BACKGROUND_COLOR,
    BUTTON_SUBMIT_UNDERLAY_COLOR,
    BUTTON_SUBMIT_TITLE_COLOR,

    FORM_HEADER_TITLE,
    TEXT_INPUT_BORDER_COLOR,
    CHECK_BOX_FIELD_COLOR,
    CHECK_BOX_BORDER_COLOR,
} from '@assets/theme';

const containerHeight = (Dimensions.get('window').height - Navigator.NavigationBar.Styles.General.TotalNavHeight) * .9;
const containerWidth = (Dimensions.get('window').width);
const formContainerWidth = (Dimensions.get('window').width > 500) ? 420 : 240;

const screenStyles = {
    screenContainerBG: {
        backgroundColor: SCREEN_CONTAINER_BACKGROUND_COLOR,
    },
    screenHeaderBG: {
        backgroundColor: SCREEN_HEADER_BACKGROUND_COLOR,
    },
    screenHeaderTitle: {
        color: SCREEN_HEADER_TITLE,
    },
    screenHeaderH1: {
        color: SCREEN_HEADER_TITLE,
        fontWeight: 'bold',
        fontSize: 40,
        padding: 20,
        paddingTop: 100,
    },
    screenContainerMaxHeight: {
        height: containerHeight,
    },
    screenContainerCenter: {
        alignItems: 'center',
        alignSelf: 'center',
    },
    screenContainerEmptyItem: {
        flex:1,
    },
    screenContainerColumn: {
        flex: 1,
        flexDirection: 'row',
    }
};

const buttonStyles = {
    customButton: {
        backgroundColor: BUTTON_SUBMIT_BACKGROUND_COLOR,
        minHeight:60,
        borderRadius: 5,
        margin: 5,
        padding: 5,
    },
    customButtomTitle: {
        color: BUTTON_SUBMIT_TITLE_COLOR,
        fontWeight:'bold',
        fontSize:20,
        alignSelf: 'center',
        lineHeight:43,
    }
};

const formStyles = {
    formContainer: {
        width: formContainerWidth,
    },
    formHeaderContainer: {
        minWidth:100,
        minHeight:130,
        maxHeight:80,
    },
    formHeaderImage: {
        flex: 1,
        width: null,
        height: null,
        resizeMode: 'contain',
    },
    formHeaderTitle: {
        alignSelf: 'center',
        textAlign:'center',
        color:FORM_HEADER_TITLE,
        fontWeight:'bold',
        fontSize:18,
    },

    textInputContainer: {
        marginBottom:10,
        marginTop:30,
        marginRight:5,
        marginLeft:5,
    },
    textInputColumnContainer: {
        flexDirection: 'row',
    },
    textInputDefault: {
        borderColor: TEXT_INPUT_BORDER_COLOR,
        borderWidth: 1,
        borderStyle: 'solid',
        borderRadius: 5,
        marginBottom: 20,
        fontSize:20,
    },

    checkboxFieldContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        padding: 5,
    },
    checkboxFieldLabel: {
        color: CHECK_BOX_FIELD_COLOR,
        paddingLeft: 10,
        fontSize: 20,
    },
    checkboxDefault: {
        width: 30,
        height: 30,
        borderWidth: 2,
        borderColor: CHECK_BOX_BORDER_COLOR,
        borderRadius: 3
    },
};

const stylesGlobal = StyleSheet.create(
    Object.assign(
        screenStyles,
        buttonStyles,
        formStyles,
    )
);
export default stylesGlobal;
