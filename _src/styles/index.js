import {
    Platform,
    Dimensions,
    PixelRatio
} from "react-native";

import stylesGlobal     from './global.js';
import stylesAndroid    from './android.js';
import stylesIOS        from './ios.js';
import stylesWeb        from './web.js';

let styles = Object.assign({}, stylesGlobal, stylesIOS, stylesAndroid, stylesWeb);

if (
    Platform.OS === 'android' &&
    Platform.Version >= 17 &&
    PixelRatio.get() == 2 &&
    Dimensions.get('window').width >= 1024 &&
    Dimensions.get('window').height >= 720 &&
    process.env.PLATFORM_ENV === 'android' &&
    process.env.NODE_ENV === 'development'
) {
    // update styles
}

export default styles;

